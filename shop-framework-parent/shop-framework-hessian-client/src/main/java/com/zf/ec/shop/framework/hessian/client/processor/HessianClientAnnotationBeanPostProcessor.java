package com.zf.ec.shop.framework.hessian.client.processor;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Properties;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.io.ClassPathResource;
import org.springframework.remoting.caucho.HessianProxyFactoryBean;
import org.springframework.stereotype.Component;
import org.springframework.util.ReflectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.context.support.XmlWebApplicationContext;

import com.zf.ec.shop.framework.hessian.client.annotation.HessianClient;
import com.zf.ec.shop.service.api.IService;

/**
 * 
 * Bean post processor to generate hessian client proxy factory bean.
 * @see com.zf.ec.shop.framework.hessian.client.annotation.HessianClient
 * 
 * @author amos
 *
 */
@Component
public class HessianClientAnnotationBeanPostProcessor extends AutowiredAnnotationBeanPostProcessor implements ApplicationContextAware{
    
    private static Log log = LogFactory.getLog(HessianClientAnnotationBeanPostProcessor.class);
    
    private static final String GENERATE_HESSIAN_PROXY_CONFIG_NAME = "com.zf.ec.shop.framework.hessian.client.annotation.HessianClient.isGenerateHessianProxy";
    private static final String CONFIG_PROPERTIES_FILE = "shop-framework-hessian-client-config.properties";
    private static final String GENERATE_HESSIAN_PROXY_CONFIG_TRUE = "true";
    private static final String SERVICE_URL_PROPERTY_KEY_PREFIX = "shop-framework-hessian-client-config.serviceUrl.";
    
    private static Class<HessianClient> ANNOTATION_CLASS_TO_PROCESS = HessianClient.class;
    
    private static boolean isGenerateHessianProxy = false;
    private static Properties properties;
    
    private ApplicationContext applicationContext;
    
    static{
        initialize();
    }
    
    private static final void initialize(){
        
        ClassPathResource resource = new ClassPathResource(CONFIG_PROPERTIES_FILE);
        
        try {
            properties = new Properties();
            properties.load(resource.getInputStream());
            
        } catch (IOException e) {
            log.error("Error reading properties file from resource path '"+ CONFIG_PROPERTIES_FILE
                    + "', the action of annotation '" + HessianClient.class.toString() 
                    + "' will default to not generate hessian proxy, but will be the same as annotation '"
                    + Autowired.class.toString() +"'! The exception detail is: " + ExceptionUtils.getStackTrace(e));
        }
        
        if(GENERATE_HESSIAN_PROXY_CONFIG_TRUE.equals(properties.getProperty(GENERATE_HESSIAN_PROXY_CONFIG_NAME))){
            isGenerateHessianProxy = true;
        }
        
    }
    
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        if(!isGenerateHessianProxy){
            log.info("HessianClientAnnotationBeanPostProcessor is disabled, field annotated with HessianClient"
                    + " will be treated as 'org.springframework.beans.factory.annotation.Autowired'!");
            return super.postProcessBeforeInitialization(bean, beanName);
        }
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(final Object bean, final String beanName) throws BeansException {
        
        if(!isGenerateHessianProxy){
            return super.postProcessAfterInitialization(bean, beanName);
        }
        
        ReflectionUtils.doWithLocalFields(bean.getClass(), new ReflectionUtils.FieldCallback(){

            @Override
            public void doWith(Field field) throws IllegalArgumentException, IllegalAccessException {
                
                HessianClient annotation = field.getAnnotation(ANNOTATION_CLASS_TO_PROCESS);
                if(annotation == null){
                    return;
                }
                
                logger.info("Generating hessian proxy bean for field: " + field.toString() + ".");
                
                if(Modifier.isStatic(field.getModifiers()) || Modifier.isFinal(field.getModifiers())){
                    throw new IllegalArgumentException("Annotation '" + HessianClient.class.toString()
                            + "' doesn't support a static field or a final field! "
                            +"The class of the field declaring this annotation is '" 
                            + field.getDeclaringClass().toString() + "'.");
                }
                
                String proxyBeanName = annotation.beanName();
                String serviceUrlKey = SERVICE_URL_PROPERTY_KEY_PREFIX + annotation.serviceUrlKey();
                
                if(StringUtils.isEmpty(serviceUrlKey)){
                    throw new IllegalArgumentException("Value of 'serviceUrlKey' of annotation '" 
                            + HessianClient.class.toString() + "' mustn't be empty! "
                            +"The class of the field declaring this annotation is '" 
                            + field.getDeclaringClass().toString() + "'.");
                }
                
                BeanDefinitionBuilder beanDefinitionBuilder = BeanDefinitionBuilder.genericBeanDefinition(HessianProxyFactoryBean.class);
                
                String serviceUrl = properties.getProperty(serviceUrlKey);
                
                if(StringUtils.isEmpty(serviceUrl)){
                    throw new IllegalArgumentException("Value of 'serviceUrlKey' of annotation '" 
                            + HessianClient.class.toString() + "' may be incorrect! Couldn't find key '"
                            + (SERVICE_URL_PROPERTY_KEY_PREFIX + serviceUrlKey) + "' defined in properties file '"
                            + CONFIG_PROPERTIES_FILE + "' in classpath resource. The class of the field declaring "
                            + "this annotation is '" +field.getDeclaringClass().toString() + "'.");
                }
                
                boolean needDefineBean = true;
                BeanDefinitionRegistry beanDefinitionRegistry = (BeanDefinitionRegistry) ((XmlWebApplicationContext)applicationContext).getBeanFactory();
                
                if(StringUtils.isEmpty(proxyBeanName)){
                    proxyBeanName = serviceUrlKey + "$HessianProxyFactoryBean";
                }
                
                //check if the type of the existing bean with the same name in container is hessian service proxy bean.
                if(applicationContext.containsBean(proxyBeanName)){
                    BeanDefinition beanDefinition = beanDefinitionRegistry.getBeanDefinition(proxyBeanName);
                    if(!HessianProxyFactoryBean.class.getName().equals(beanDefinition.getBeanClassName())){
                        throwExceptionForExistingBeanOfOtherType(field, proxyBeanName);
                    }else{
                        needDefineBean = false;
                    }
                }
                
                if(needDefineBean){
                    
                    Class<?> fieldType = field.getType();
                    if(!isServiceInterface(fieldType)){
                       return;
                    }
                    
                    beanDefinitionBuilder.addPropertyValue("serviceInterface", fieldType);
                    beanDefinitionBuilder.addPropertyValue("serviceUrl", serviceUrl);
                    
                    //When calling postProcessAfterInitialization method, the bean isn't registered into bean factory,
                    //so check if proxyBeanName same to beanName, if yes, that means defining two beans of the same name.
                    if(proxyBeanName.equals(beanName)){
                        throwExceptionForExistingBeanOfOtherType(field, proxyBeanName);
                    }
                    
                    beanDefinitionRegistry.registerBeanDefinition(proxyBeanName, beanDefinitionBuilder.getBeanDefinition());
                }
                
                boolean isAccessible = field.isAccessible();
                if(!isAccessible){
                    field.setAccessible(true);
                }
                
                try {
                    ReflectionUtils.setField(field, bean, applicationContext.getBean(proxyBeanName));
                } catch (Exception e) {
                    String msg = "Error post processing bean of name '" + beanName + "' when processing annotation '"
                            + HessianClient.class.toString() + "'. The class of the field declaring this annotation is '"
                            + field.getDeclaringClass().toString() + "'";
                    throw new BeanInitializationException(msg,e);
                }finally{
                    if(!isAccessible){
                        field.setAccessible(false);
                    }
                }
                
            }
            
            private boolean isServiceInterface(Class<?> clazz) throws IllegalArgumentException{
                
                Class<?>[] interfaces = clazz.getInterfaces();
                for(Class<?> c : interfaces){
                    if(IService.class.isAssignableFrom(c)){
                        return true;
                    }
                }
                
                return false;
                
            }
            
            private void throwExceptionForExistingBeanOfOtherType(Field field, String proxyBeanName){
                throw new IllegalArgumentException("Cannot create bean with name '" + proxyBeanName 
                        + "' of type HessianProxyFactoryBean because of existing bean of the same name. "
                        + "Please specify a new bean name. The class of the field declaring annotation '" 
                        + HessianClient.class.getName() + "' is '"+field.getDeclaringClass().toString()+"'.");
            }
            
        });
        
        return bean;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

}
