package com.zf.ec.shop.framework.hessian.client.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 *   The annotation to generate a hessian proxy bean for a service.
 *   To use this annotation, you need to put a properties file named "shop-framework-hessian-client-config.properties"
 * into class path, otherwise, it cannot work correctly.
 *   The properties configuration file contains two kinds of configurations:
 *      1. hessian proxy settings
 *          @see com.zf.ec.shop.framework.hessian.client.processor.HessianClientAnnotationBeanPostProcessor.GENERATE_HESSIAN_PROXY_CONFIG_NAME
 *          the key must be "com.zf.ec.shop.framework.hessian.client.annotation.HessianClient.isGenerateHessianProxy"
 *          @see com.zf.ec.shop.framework.hessian.client.processor.HessianClientAnnotationBeanPostProcessor.GENERATE_HESSIAN_PROXY_CONFIG_TRUE
 *          the value can be "true" or "false"
 *      2.hessian service url configurations
 *          @see com.zf.ec.shop.framework.hessian.client.processor.HessianClientAnnotationBeanPostProcessor.SERVICE_URL_PROPERTY_KEY_PREFIX
 *          the key must start with "shop-framework-hessian-client-config.serviceUrl."
 *   If there is a problem reading properties file "shop-framework-hessian-client-config.properties" or the value of 
 * "com.zf.ec.shop.framework.hessian.client.annotation.HessianClient.isGenerateHessianProxy" isn't "true", the framework won't generate hessian
 * service proxy for the annotated field, but it will work as annotation "org.springframework.beans.factory.annotation.Autowired", that is to say,
 * you can treate this annotation as "org.springframework.beans.factory.annotation.Autowired" in this condition, but the attribute "required" will be false.
 * 
 *   NOTE: This annotation doesn't support the static field or final field, and it will cause exception thrown when annotating a static field 
 * or a final field. 
 *   
 * @author amos
 *
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface HessianClient {
    
    /**
     *   Specify the bean name of the generated hessian proxy.
     *   Default to ${interfaceName}$HessianProxyFactoryBean if empty.
     *   The ${interfaceName} is the type name of the field annotated.
     */
    public String beanName() default "";
    
    /**
     *   Specify the key defining the url of the hessian server.  
     *   The key "shop-framework-hessian-client-config.serviceUrl.${serviceUrlKey}" 
     * should be defined in the properties file "shop-framework-hessian-client-config.properties".
     */
    public String serviceUrlKey();
}
