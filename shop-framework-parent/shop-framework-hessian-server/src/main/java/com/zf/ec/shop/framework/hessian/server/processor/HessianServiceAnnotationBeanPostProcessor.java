package com.zf.ec.shop.framework.hessian.server.processor;

import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.remoting.caucho.HessianServiceExporter;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.zf.ec.shop.framework.hessian.server.annotation.HessianService;
import com.zf.ec.shop.service.api.IService;

/**
 *   The bean post processor to export a service as a hessian service.
 *   Users can redefine this bean in application context, and set isGenerateHessianService to true
 * if hope to enable this annotation. For example:
 *   <bean id="hessianServiceAnnotationBeanPostProcessor"
 *     class="com.zf.ec.shop.framework.hessian.server.processor.HessianServiceAnnotationBeanPostProcessor">
 *     <property name="generateHessianService" value="true">
 *   </bean>
 *   NOTE: This function will be disabled in default, that is to say, the hessian service bean won't be exported
 * until the default configuration of HessianServiceAnnotationBeanPostProcessor.isGenerateHessianService was overriden.
 * @see com.zf.ec.shop.framework.hessian.server.annonation.HessianService.
 * 
 * @author amos
 *
 */
@Component
public class HessianServiceAnnotationBeanPostProcessor implements BeanFactoryPostProcessor{
    
    private static final Class<HessianService> ANNOTATION_CLASS_TO_PROCESS = HessianService.class;
    
    private static final Log log = LogFactory.getLog(HessianServiceAnnotationBeanPostProcessor.class);
    
    /**
     * Defines if generate hessian service bean.
     */
    private boolean isGenerateHessianService = false;

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        
        if(!isGenerateHessianService){
            log.info("HessianServiceAnnotationBeanPostProcessor is disabled, skip generating hessian service bean!");
            return;
        }
        
        Map<String, Object> beansMap = beanFactory.getBeansWithAnnotation(ANNOTATION_CLASS_TO_PROCESS);
        
        IService service = null;
        
        for(Entry<String, Object> entry : beansMap.entrySet()){
            
            if(null == entry.getValue() || !(entry.getValue() instanceof IService)){
                continue;
            }
            
            service = (IService) (entry.getValue());
            HessianService annotation = service.getClass().getAnnotation(ANNOTATION_CLASS_TO_PROCESS);
            
            log.info("Generating hessian service for class: " + service.getClass().toString() + ".");
            
            String overridingServiceName = annotation.value();
            String actualServiceName = null;
            
            if(StringUtils.isEmpty(overridingServiceName)){
                actualServiceName = entry.getKey();
            }else{
                actualServiceName = overridingServiceName;
            }
            
            if(!actualServiceName.startsWith("/")){
                actualServiceName = "/"+actualServiceName;
            }
            
            if(beanFactory.containsBean(actualServiceName)){
                throw new IllegalArgumentException("Cannot export '" + service.getClass().toString()
                    + "' as hessian service, because of existing bean of name '" + actualServiceName + "'. "
                    + "The class declaring annotation '" + HessianService.class.toString() + "' is '"
                    + service.getClass().toString() + "'");
            }
            
            BeanDefinitionBuilder beanDefinitionBuilder = BeanDefinitionBuilder.genericBeanDefinition(HessianServiceExporter.class);
            beanDefinitionBuilder.addPropertyReference("service", entry.getKey());
            
            String serviceInterface = getServiceInterface(service.getClass());
            if(StringUtils.isEmpty(serviceInterface)){
                return;
            }
            beanDefinitionBuilder.addPropertyValue("serviceInterface", serviceInterface);

            ((BeanDefinitionRegistry)beanFactory).registerBeanDefinition(actualServiceName, beanDefinitionBuilder.getBeanDefinition());
            
        }
        
    }
    
    private String getServiceInterface(Class<? extends IService> serviceClass) throws IllegalArgumentException{
        
        Class<?>[] interfaces = serviceClass.getInterfaces();
        
        for(Class<?> clazz : interfaces){
            if(IService.class.isAssignableFrom(clazz)){
                return clazz.getName();
            }
        }
        
        throw new IllegalArgumentException();
        
    }

    public void setGenerateHessianService(boolean isGenerateHessianService) {
        this.isGenerateHessianService = isGenerateHessianService;
    }
}