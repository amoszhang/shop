package com.zf.ec.shop.framework.hessian.server.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * The annonation to export a service as a hessian service.
 * The url of the hessian service will be mapped as ${contextPath}/${hessianServiceName}.
 * The service bean must be an instance of {@link com.zf.ec.shop.service.api.IService}.
 * The default value of hessianServiceName will be the service bean name if not specified.
 * To specify the hessianServiceName, use value attribute.
 * 
 *   Users can redefine this bean in application context, and set isGenerateHessianService to false
 * if hope to disable this annotation. For example:
 *   <bean id="hessianServiceAnnotationBeanPostProcessor"
 *     class="com.zf.ec.shop.framework.hessian.server.processor.HessianServiceAnnotationBeanPostProcessor">
 *     <property name="generateHessianService" value="false">
 *   </bean>
 * 
 * @author amos
 *
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface HessianService {
    
    /**
     * Specify the hessian service name.
     * Default to the service bean name if not specified.
     */
    String value() default "";
}