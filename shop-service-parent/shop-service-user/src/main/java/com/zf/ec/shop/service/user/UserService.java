package com.zf.ec.shop.service.user;

import org.springframework.stereotype.Service;

import com.zf.ec.shop.framework.hessian.server.annotation.HessianService;
import com.zf.ec.shop.service.api.user.IUserService;

@Service
@HessianService
public class UserService implements IUserService{

}   