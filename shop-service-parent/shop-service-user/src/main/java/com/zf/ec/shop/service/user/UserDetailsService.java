package com.zf.ec.shop.service.user;

import java.util.Arrays;
import java.util.Collections;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.zf.ec.shop.framework.hessian.server.annotation.HessianService;
import com.zf.ec.shop.model.user.User;
import com.zf.ec.shop.service.api.user.IUserDetailsService;

@Service
@HessianService
public class UserDetailsService implements IUserDetailsService{

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
       if("zf".equals(username)){
           return new User("zf", "zf", Collections.unmodifiableList(Arrays.asList(new GrantedAuthority[]{
                   new SimpleGrantedAuthority("ROLE_ADMIN"), new SimpleGrantedAuthority("ROLE_USER")
           })));
       }else{
           throw new UsernameNotFoundException("");
       }
    }

}
