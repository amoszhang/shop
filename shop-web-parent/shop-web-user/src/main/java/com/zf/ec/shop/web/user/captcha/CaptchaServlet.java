package com.zf.ec.shop.web.user.captcha;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.github.cage.Cage;
import com.github.cage.IGenerator;

public class CaptchaServlet extends HttpServlet {

    private static final long serialVersionUID = 5040058290147253822L;
    
    private static final String SESSION_ATTR_NAME_CAPTCHA_CODE = "_capcha_code";
    
    private Cage cage;
    
    @Override
    public void init(ServletConfig servletConfig){
        cage = new Cage();
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException{
        
        String captchaText = new CaptchaTextGenerator().next();
        request.getSession().setAttribute(SESSION_ATTR_NAME_CAPTCHA_CODE, captchaText);
        setResponseHead(response);
        synchronized(this){
            cage.draw(captchaText, response.getOutputStream());
        }
        
    }

    private void setResponseHead(HttpServletResponse response){
        response.setContentType("image/" + cage.getFormat());
        response.setHeader("Cache-Control", "no-cache, no-store");
        response.setHeader("Pragma", "no-cache");
        long time = System.currentTimeMillis();
        response.setDateHeader("Last-Modified", time);
        response.setDateHeader("Date", time);
        response.setDateHeader("Expires", time);
    }
}

class CaptchaTextGenerator implements IGenerator<String>{
    
    @Override
    public String next() {
        return Long.toString(10000+(int)(Math.random()*10000)%90000);
    }
    
}
