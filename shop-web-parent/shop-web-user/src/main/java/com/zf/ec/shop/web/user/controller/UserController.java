package com.zf.ec.shop.web.user.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.zf.ec.shop.framework.hessian.client.annotation.HessianClient;
import com.zf.ec.shop.service.api.user.IUserService;

@Controller
public class UserController {
    
    @HessianClient(serviceUrlKey = "userService")
    private IUserService userService;

    @RequestMapping("/login")
    public String login(HttpServletRequest request){
        return "login";
    }
    
}
