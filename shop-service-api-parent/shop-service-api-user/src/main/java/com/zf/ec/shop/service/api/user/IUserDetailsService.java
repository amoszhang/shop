package com.zf.ec.shop.service.api.user;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.zf.ec.shop.service.api.IService;

public interface IUserDetailsService extends UserDetailsService, IService {

}
