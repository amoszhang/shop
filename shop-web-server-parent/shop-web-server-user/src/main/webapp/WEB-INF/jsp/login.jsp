<%@ page contentType="text/html;charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <title>User Login</title>
    <meta http-equiv="Content-Type" content="text/html; charset=gb2312">
  </head>
  <body>
    <c:if test="${param.login_failure == 'true'}">
      <span style="color:red">Username, password or validation code not correct, please check and login again! </span>
    </c:if>
    <form name='loginForm' action="<c:url value='login-process'/>" method='POST'>
      Username: <input type="text" name="username"><br>
      Password: <input type="password" name="password"><br>
      Validation code: <input type="text" name="validationCode">
      &nbsp;&nbsp;&nbsp;&nbsp;
     <img src="${pageContext.request.contextPath}/servlet/captcha" 
        onclick="this.src='${pageContext.request.contextPath}/servlet/captcha?random='+Math.random();"
        style="cursor:pointer"/><br>
      <input id="remember-me" name="remember-me" type="checkbox" value="true">
      <label for="remember-me">Remember me!</label> <br>
      <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
      <input type="submit" value="Submit">
      &nbsp;&nbsp;&nbsp;&nbsp;
      <input type="reset" value="reset"><br>
    </form>
  </body>
</html>
